﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class AUTOesController : Controller
    {
        private testEntities db = new testEntities();

        // GET: AUTOes
        public ActionResult Index()
        {
            var aUTO = db.AUTO.Include(a => a.MODELO);
            return View(aUTO.ToList());
        }

        // GET: AUTOes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTO aUTO = db.AUTO.Find(id);
            if (aUTO == null)
            {
                return HttpNotFound();
            }
            return View(aUTO);
        }

        // GET: AUTOes/Create
        public ActionResult Create()
        {
            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO");
            return View();
        }

        // POST: AUTOes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_AUTO,ID_MODELO,PATENTE,ANO,COLOR,OBSERVACION")] AUTO aUTO)
        {
            if (aUTO.ANO > 2020)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if(aUTO.ANO < 1990)
                    {
                        aUTO.OBSERVACION = "ANTIGUO";
                    }
                    db.AUTO.Add(aUTO);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

                ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO", aUTO.ID_MODELO);
                return View(aUTO);
        }

        // GET: AUTOes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTO aUTO = db.AUTO.Find(id);
            if (aUTO == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO", aUTO.ID_MODELO);
            return View(aUTO);
        }

        // POST: AUTOes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_AUTO,ID_MODELO,PATENTE,ANO,COLOR,OBSERVACION")] AUTO aUTO)
        {
            if (ModelState.IsValid)
            {
                if (aUTO.ANO < 1990)
                {
                    aUTO.OBSERVACION = "ANTIGUO";
                }
                db.Entry(aUTO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO", aUTO.ID_MODELO);
            return View(aUTO);
        }

        // GET: AUTOes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTO aUTO = db.AUTO.Find(id);
            if (aUTO == null)
            {
                return HttpNotFound();
            }
            return View(aUTO);
        }

        // POST: AUTOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AUTO aUTO = db.AUTO.Find(id);
            db.AUTO.Remove(aUTO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
